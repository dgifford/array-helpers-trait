<?php
Namespace dgifford\Traits;

/*
	General purpose methods for manipulating arrays.

 

    Copyright (C) 2016  Dan Gifford

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 
trait ArrayHelpersTrait
{
	/**
	 * Returns true if every element of the array is a string.
	 * 
	 * @param  array  $arr
	 * @return boolean
	 */
	public function isArrayOfStrings( $arr = [] )
	{
		if( is_array($arr) )
		{
			foreach( $arr as $value )
			{
				if( is_string($value) === false )
				{
					return false;
				}
			}

			return true;
		}

		return false;
	}



	/**
	 * Boolean test if a array is multi-dimensional (contains other arrays).
	 * Achieves this by examining each element in an array and returning true
	 * as soon as one is an array.
	 * 
	 * @param  array   $arr
	 * @return boolean
	 */
	public function isMultiDimensional( $arr = [] )
	{
		if( is_array($arr) )
		{
			foreach( $arr as $key => $value )
			{
				if( is_array($value) )
				{
					return true;
				}
			}
		}

		return false;
	}



	/**
	 * Returns true if all indexes are integers.
	 * 
	 * @param  array   $arr
	 * @return boolean
	 */
	public function isNumericallyIndexed( $arr = [] )
	{
		if( is_array($arr) and !empty($arr) )
		{
			foreach( $arr as $key => $value )
			{
				if( !is_int($key) )
				{
					return false;
				}
			}

			return true;
		}

		return false;
	}



	/**
	 * Returns the md5 hash of an array
	 * http://stackoverflow.com/questions/2254220/php-best-way-to-md5-multi-dimensional-array#answer-7723730
	 * @return string 	md5 hash
	 */
	public function md5Array( $arr = [] )
	{
		return md5(json_encode($arr));
	}



	/**
	 * Flattens a multi-dimensional array into a flat one.
	 * http://stackoverflow.com/questions/1319903/how-to-flatten-a-multidimensional-array
	 * 
	 * @param  array   $arr
	 * @return array
	 */
	public function flattenArray( $arr = [] )
	{
		$return = array();
		array_walk_recursive($arr, function($a) use (&$return) { $return[] = $a; });
	    return $return;
	}



	/**
	 * Makes a value into an array if it isn't already.
	 * Passing a second boolean parameter as true will force it into an array even if it is already.
	 * 
	 * @param  mixed   $arr
	 * @return array
	 */
	public function makeArray( $value, $force = false )
	{
		if( !is_array($value) or $force == true )
		{
			$value = array( $value );
		}

		return $value;
	}



	/**
	 * Turns an array into a string of html style attributes.
	 * A value which is an array will be converted into a space-seperated string (e.g. a list of CSS classes).
	 * A value which is a boolean will only be used if true (e.g. selected="selected").
	 * A numerically indexed item in an array will result in only the value being used (e.g. checked)
	 * @param  array  $arr 
	 * @return string    	  Attributes, e.g. class="my_class" type="text"
	 */
	public function arrayToAttributes( $arr = [] )
	{
		$output = [];

		foreach( $arr as $name => $value )
		{
			// Convert array value to space seperated string (e.g. classes)
			if( is_array( $value ) )
			{
				$value = implode( ' ', $value );
			}

			// Deal with boolean values
			if( $value === true )
			{
				$value = $name;
			}

			// Add value if it is a string
			if( is_string($name) )
			{
				$output[] = "$name=\"{$value}\"";
			}
			elseif( is_string($value) )
			{
				$output[] = "{$value}";
			}
		}

		if( !empty( $output ) )
		{
			return " " . implode( ' ', $output );
		}

		return '';
	}



	/**
	 * Simple convsersion from array to string using a mask containing one or more keys from the array.
	 * Strings in the mask that match {key} will be replaced by the corresponding value.
	 * The { and } characters can be changed by providing the $start and $end strings.
	 * All instances of the keys will be replaced, any that are not replaced are removed.
	 *
	 * 
	 * @param  array  $arr 
	 * @param  string $mask  
	 * @param  string $start 
	 * @param  string $end
	 * @return string
	 */
	public function arrayToString( $arr, $mask, $start = '{', $end = '}' )
	{
		if( !is_array( $arr ) or !is_string( $mask ) )
		{
			throw new \InvalidArgumentException('Invalid array or mask' );
		}

		$needles = array_map( function($value) use ($start, $end) { return $start . $value . $end; }, array_keys( $arr ) );

		return preg_replace( '/' . preg_quote($start) . ".*?" . preg_quote($end) . '/', '', str_replace( $needles, array_values($arr), $mask ) );
	}



	/**
	 * Converts a string into an array.
	 * The string is split at any of these characters: , | ; \n
	 * Empty elements in the array are discarded.
	 * All elements in the array are trimmed of whitespace.
	 *
	 * @param string $string 	The string to convert
	 * @param bool $no_empty 	Whether to remove empty elements
	 * @param bool $trim 		Whether to trim whitespace from all elements
	 * @param array $delimiters Array of delimimters that define where the string should be split
	 * @return array
	 */
	public function stringToArray( $string = '', $no_empty = true, $trim = true, $delimiters = [',','|',';', "\n"] )
	{
		if( !is_string($string) )
		{
			return $string;
		}

		$string = str_replace($delimiters, '^,^', $string);

		$result = explode('^,^', $string);

		if( $trim )
		{
			$result = array_map('trim', $result);
		}

		if( $no_empty )
		{
			$result = array_filter($result);
		}
		
		return $result;
	}



	/**
	 * Remove elements with a null value from an array and optionally reset 
	 * numeric indexes.
	 * @param  array   $arr        
	 * @param  boolean $reset_keys
	 * @return array
	 */
	public function removeNullElements( $arr = [], $reset_keys = true )
	{
		// Remove any arguments set as null
		foreach( $arr as $key => $value )
		{
			if( is_null($value) )
			{
				unset($arr[$key]);
			}
		}

		if( $reset_keys )
		{
			$arr = array_values( $arr );
		}
		
		return $arr;
	}



	/**
	 * Convert an object to an array.
	 * 
	 * @param  object $object [description]
	 * @return array
	 */
	public function objectToArray( $object )
	{
		if( is_object( $object ) )
		{
			return json_decode( json_encode($object), true );
		}

		return $object;
	}
}