<?php
Namespace dgifford\Traits;



/**
 * Auto Loader
 * 
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class Foo
{
	Use ArrayHelpersTrait;

	public $associative_array_of_strings = [
		'one' => 'one',
		'two' => 'two',
		'three' => 'three',
		'four' => 'four',
	];

	public $multi_dimensional_array = [
		'one',
		'two',
		['three', 'four', 'five', ['six', 'seven'] ],
		'eight',
	];

	public $mixed_array =
	[
		'one',
		'two',
		'third' => 'three', 
		'fourth' => 'four', 
		'five',
		false,
	];
}



class ArrayHelpersTraitTest extends \PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		$this->foo = New Foo;
	}



	public function testIsNumericallyIndexed()
	{
		$this->assertFalse( $this->foo->isNumericallyIndexed( $this->foo->associative_array_of_strings ) );

		$this->assertTrue( $this->foo->isNumericallyIndexed( $this->foo->multi_dimensional_array ) );
		
		$this->assertFalse( $this->foo->isNumericallyIndexed( $this->foo->mixed_array ) );
	}



	public function testFlatArrayIsMultiDimensional()
	{
		$this->assertFalse( $this->foo->isMultiDimensional( $this->foo->associative_array_of_strings ) );
	}



	public function testMultiDimensionalArrayIsMultiDimensional()
	{
		$this->assertTrue( $this->foo->isMultiDimensional( $this->foo->multi_dimensional_array ) );
	}



	public function testFlattedMultiDimensionalArray()
	{
		$this->assertSame( ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight'], $this->foo->flattenArray( $this->foo->multi_dimensional_array ) );
	}



	public function testMakeStringArray()
	{
		$this->assertSame( ['wibble'], $this->foo->makeArray( 'wibble' ) );
	}



	public function testMakeArrayArray()
	{
		$this->assertSame( ['wibble'], $this->foo->makeArray( ['wibble'] ) );
	}



	public function testMakeArrayArrayForced()
	{
		$this->assertSame( [['wibble']], $this->foo->makeArray( ['wibble'], true ) );
	}



	public function testArrayToString()
	{
		$this->assertSame( 'four three two one two one ', $this->foo->arrayToString( $this->foo->associative_array_of_strings, '{four} {three} {arse}{two} {one} {two} {one} {none}' ) );
	}



	public function testSetArrayToString()
	{
		$this->assertSame( 'four three two one two one ', $this->foo->arrayToString( $this->foo->associative_array_of_strings, '{four} {three} {arse}{two} {one} {two} {one} {none}' ) );
	}



	public function testIsArrayOfStrings()
	{
		$this->assertTrue(  $this->foo->isArrayOfStrings( $this->foo->associative_array_of_strings ) );
		$this->assertFalse( $this->foo->isArrayOfStrings( $this->foo->multi_dimensional_array ) );
		$this->assertFalse( $this->foo->isArrayOfStrings( $this->foo->mixed_array ) );
		$this->assertFalse( $this->foo->isArrayOfStrings( 'foo' ) );
	}
}